const mongoose = require('mongoose');

const MealSchema = new mongoose.Schema({
    userid: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    calories: {
        type: String,
        required: true
    },
    carbs: {
        type: String,
        required: true
    },
    fat: {
        type: String,
        required: true
    },
    protein :{
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    }
});

const Meal = mongoose.model('Meal', MealSchema);

module.exports = Meal;
