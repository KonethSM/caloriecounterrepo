const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    premium :{
        type: Boolean,
        default: false
    },
    dailycals : {
        type: Number,
        default: 0
    },
    dailycarbs : {
        type: Number,
        default: 0
    },
    dailyfat : {
        type: Number,
        default: 0
    },
    dailyprotein : {
        type: Number,
        default: 0
    },
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
