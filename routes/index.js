const {getFullDate,getAllWeekDays} = require("../scripts/dateFinder");
const express = require('express');
const router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');
const Meal = require('../models/Meal');
process.env.USDA_NDB_API_KEY = "eh3F2EjJXNBeWktpuSxQoJjdYXhyKYJmDTgXLMS4";


// Welcome Page
router.get('/', forwardAuthenticated, (req, res) => res.render('welcome'));

// Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) =>
    Meal.find({userid: req.user._id}, (err,response) =>{
        let caloriesLeft = req.user.dailycals;  
        let meals = []      
        response.forEach(item =>{
            if(getFullDate(item.date) === getFullDate(new Date)){
                caloriesLeft -= item.calories;
                meals.push(item);
            }
                
        })
        res.render('dashboard', {
            user: req.user,
            remaining: caloriesLeft,
            meals: meals
        })
    })
    
);

// Meals
router.get('/meals', ensureAuthenticated, (req, res) =>

    Meal.find({ userid: req.user._id }, function (err, response) {
        //console.log(response);
        
        //console.log(new Date().toString() );
        var now = new Date();

        var todaysMeals = [];
        
        //response.forEach(meal => console.log(meal.date.toISOString().substring(0,10)));
        response.forEach(meal => {
            if(meal.date.toISOString().substring(0,10) == now.toISOString().substring(0,10)) {
                todaysMeals.push(meal);
            }
        });

        var goals = {cals: parseInt(req.user.dailycals), carbs: parseInt(req.user.dailycarbs), fat: parseInt(req.user.dailyfat), prot: parseInt(req.user.dailyprotein)};
        
        todaysMeals.forEach(ele => {
            goals.cals -= parseInt(ele.calories);
            goals.carbs -= parseInt(ele.carbs);
            goals.fat -= parseInt(ele.fat);
            goals.prot -= parseInt(ele.protein);
        });

        // goals.cals = parseInt(req.user.dailycals) - goals.cals;
        // goals.carbs = parseInt(req.user.dailycarbs) - goals.carbs;
        // goals.fat = parseInt(req.user.dailyfat) - goals.fat;
        // goals.prot = parseInt(req.user.dailyprotein) - goals.prot;
      
        //console.log(goals);
       
        res.render('meals', {
            user: req.user,
            meals: response,
            date: now.toISOString().substring(0,10),
            goals: goals
        })
    })

);

// progress
router.get('/addmeal', ensureAuthenticated, async (req, res) =>
    res.render('addmeal', {
        user: req.user,
        meals: null,
        date: null
    })
);

// progress
router.get('/progress', ensureAuthenticated, (req, res) =>
    res.render('progress', {
        user: req.user
    })
);

// goals
router.get('/goals', ensureAuthenticated, (req, res) =>
    res.render('goals', {
        user: req.user
    })
);

// proupgrade
router.get('/proupgrade', ensureAuthenticated, (req, res) =>
    res.render('proupgrade', {
        user: req.user
    })
);

// reports
router.get('/reports', ensureAuthenticated, (req, res) => {
    Meal.find({ userid: req.user._id }, (err, response) => {
        const date = req.body.date
        let week = getAllWeekDays(new Date(date ? date : new Date()));
        res.render('reports', {
            user: req.user,
            meals: response,
            weekDays: week,
            date: date ? date : new Date()
        });
    });
});

// settings
router.get('/settings', ensureAuthenticated, (req, res) =>
    res.render('settings', {
        user: req.user
    })
);

module.exports = router;
