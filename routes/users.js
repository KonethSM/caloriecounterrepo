const {findSunday,getAllWeekDays} = require("../scripts/dateFinder");
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
// Load User model
const User = require('../models/User');
// Load meal model 
const Meal = require('../models/Meal');
const { forwardAuthenticated } = require('../config/auth');
const req = require('request-promise');
const { response } = require('express');

// NutritionFacts 
const NutritionFacts = require('nutrition-facts').default
const NF = new NutritionFacts(process.env.USDA_NDB_API_KEY);
const fetch = require('node-fetch');

// Login Page
router.get('/login', forwardAuthenticated, (req, res) => res.render('login'));

// Register Page
router.get('/register', forwardAuthenticated, (req, res) => res.render('register'));

// Register
router.post('/register', (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];

    if (!name || !email || !password || !password2) {
        errors.push({ msg: 'Please enter all fields' });
    }

    if (password != password2) {
        errors.push({ msg: 'Passwords do not match' });
    }

    if (password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters' });
    }

    if (errors.length > 0) {
        res.render('register', {
            errors,
            name,
            email,
            password,
            password2
        });
    } else {
        User.findOne({ email: email }).then(user => {
            if (user) {
                errors.push({ msg: 'Email already exists' });
                res.render('register', {
                    errors,
                    name,
                    email,
                    password,
                    password2
                });
            } else {
                const newUser = new User({
                    name,
                    email,
                    password
                });

                // Hash password
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        // Set password to hashed
                        newUser.password = hash;
                        // Save user
                        newUser
                            .save()
                            .then(user => {
                                req.flash(
                                    'success_msg',
                                    'You are now registered and can log in'
                                );
                                res.redirect('/users/login');
                            })
                            .catch(err => console.log(err));
                    });
                });
            }
        });
    }
});

// Login
router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req, res, next);
});

// Logout
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
});

// Caulcate caloric intake
router.post('/calculate', (req, res) => {
    const { age, weight, height, goal, gender } = req.body;
    var { bmr, dailycals, dailycarbs, dailyfat, dailyprotein } = 0;
    let errors = [];

    if (!age || !weight || !height || !goal) {
        errors.push({ msg: 'Please enter all fields' });
    }
    if (errors.length > 0) {
        res.render('goals', {
            user: req.user,
            errors
        });
    } else {
        console.log(age, weight, height, goal, gender);
        if (gender === 'male'){
            bmr = (4.536*weight) + (15.88*height) - (5*age) + 5;
        } else{
            bmr = (4.536*weight) + (15.88*height) - (5*age) - 161;
        }
        if (goal == 'lose'){
            dailycals = Math.round(bmr-500);
        }
        else {
            dailycals = Math.round(bmr+500);
        }
        dailyprotein = Math.round((dailycals * 0.25) * 0.20);
        dailycarbs = Math.round((dailycals * 0.25) * 0.50);
        dailyfat = Math.round((dailycals * 0.11) * 0.30);

        console.log(dailycals, dailyprotein, dailycarbs, dailyfat);
        User.findOneAndUpdate({email: req.user.email},{dailycals: dailycals, dailycarbs: dailycarbs, dailyfat: dailyfat, dailyprotein: dailyprotein},(err,doc)=>{
            if (err){
                errors.push({msg:"Unable to calculate goals"});
                res.render('goals',{
                    user: req.user,
                    errors
                })
            }
            else{
                req.flash('success_msg', 'Goals updated');
                res.redirect('/goals');
            }
        });
    }
});

// Goals
router.post('/goals', (req, res) => {
    const { dailycals, dailycarbs, dailyfat, dailyprotein } = req.body;
    let errors = [];

    if (!dailycals || !dailycarbs || !dailyfat || !dailyprotein) {
        errors.push({ msg: 'Please enter all fields.' });
    }
    if (Number.isInteger(dailycals) || Number.isInteger(dailycarbs) || Number.isInteger(dailyfat) || Number.isInteger(dailyprotein)) {
        errors.push({ msg: 'Fields must be a number.' });
    }
    if (parseInt(dailycals)<1 || parseInt(dailycarbs)<1 || parseInt(dailyfat)<1 || parseInt(dailyprotein)<1) {
        errors.push({ msg: 'Fields must be non-negative and above 0.' });
    }
    if (errors.length > 0) {
        res.render('goals', {
            user: req.user,
            errors
        });
    } else {
        User.findOneAndUpdate({email: req.user.email},{dailycals: dailycals, dailycarbs: dailycarbs, dailyfat: dailyfat, dailyprotein: dailyprotein},(err,doc)=>{
            if (err){
                errors.push({msg:"Unable to update goals"});
                res.render('goals',{
                    user: req.user,
                    errors
                })
            }
            else{
                req.flash('success_msg', 'Goals updated');
                res.redirect('/goals');
            }
        });
    }
});

//upgrade to pro
router.post('/upgrade', (req, res) => {
    const { ccname, ccnumber, ccexpiration, cvv } = req.body;
    let errors = [];

    if (!ccname || !ccnumber || !ccexpiration || !cvv) {
        errors.push({ msg: 'Please enter all fields' });
    }

    if (ccnumber.length < 16 || ccnumber.length > 16){
        errors.push({msg : "Credit card must be 16 digits long with no spaces"});
    }

    if (ccexpiration.length < 5){
        errors.push({msg : "Expiration must only have five characters format is 'xx/xx'"});
    }

    if(cvv.length < 3){
        errors.push({msg : "CVV was too short"});
    }

    if(cvv.length > 3){
        errors.push({msg : "CVV was too long"});
    }
    if (errors.length > 0) {
        res.render('proupgrade', {
            user: req.user,
            errors           
        });
    } else {
        User.findOneAndUpdate({email: req.user.email},{premium: true},{new:true},(err,doc)=>{
            if(err){
                errors.push({msg:"Was unable to upgrade to premium please contact system admin or customer support"});
                res.render('proupgrade',{
                    user: req.user,
                    errors 
                })
            }
            console.log(req.user)
            console.log(doc);
            res.render('proupgrade',{
                user: doc,
                errors 
            });
        });
    }
});

//Delete a meal
router.post('/deletemeal/:id', (req, res) => {
    Meal.findOneAndRemove({
        _id: req.params.id
    }, (err) => {
        if (err) throw err;
    });
    res.redirect('/meals');
});

router.post('/updatemeal', (req, res) => {

    const { name, cals, carbs, fat, protein, user, id1, date1 } = req.body;
    let errors = [];
    var now = new Date();

    if (!name || !cals || !carbs || !fat ||  !protein || !user) {
        errors.push({ msg: 'Please enter all fields.' });
    }

    if (name.length < 0){
        errors.push({msg : "Name must be entered"});
    }

    if (cals < 1){
        errors.push({msg : "Calories must be entered"});
    }

    if (carbs < 1){
        errors.push({msg : "Carbs must be entered"});
    }

    if (fat < 1){
        errors.push({msg : "Fat must be entered"});
    }

    if (protein < 1){
        errors.push({msg : "Protein must be entered"});
    }
    if (errors.length > 0) {
        res.redirect('/meals')
    } else {
        Meal.findOneAndUpdate({_id: req.body.id1},{calories:cals, carbs:carbs, fat:fat, protein:protein}, (err,doc) => {
            if(err){
                errors.push({msg:"Was unable to upgrade to edit the meal"});
                Meal.find({userid: req.user._id}, function(err, response) {
                    res.render('meals', {
                        user: req.user,
                        meals: response,
                        date: req.body.date1
                    })
                })
            }
            res.redirect('/meals')
        });
    }
});

router.post('/reports',(req,res)=>{
    Meal.find({userid: req.user._id}, function(err, response) {
        let fixedDay = req.body.date.replace('-','/');
        fixedDay = fixedDay.replace('-','/')
        let week = getAllWeekDays(new Date(fixedDay));
        res.render('reports', {
            user: req.user,
            meals: response,
            weekDays: week,
            date: fixedDay
        })
    })
});
// meals 
router.post('/meals', (req, res) => {
    Meal.find({userid: req.user._id}, function(err, response) {

        var todaysMeals = [];
        console.log(req.body.date);
        response.forEach(meal => {
            if(meal.date.toISOString().substring(0,10) == req.body.date) {
                todaysMeals.push(meal);
            }
        });

        var goals = {cals: parseInt(req.user.dailycals), carbs: parseInt(req.user.dailycarbs), fat: parseInt(req.user.dailyfat), prot: parseInt(req.user.dailyprotein)};
        
        todaysMeals.forEach(ele => {
            goals.cals -= parseInt(ele.calories);
            goals.carbs -= parseInt(ele.carbs);
            goals.fat -= parseInt(ele.fat);
            goals.prot -= parseInt(ele.protein);
        });

        res.render('meals', {
            user: req.user,
            meals: response,
            date: req.body.date,
            goals: goals
        })
    })

});

router.post('/addmeal', async (req, res) => {

    const { name, date, name1, user, carbs, cals, fat, protein, goals } = req.body;

    if (carbs)
    {
        var str = user.indexOf("_id");

        var ndate = new Date();

        ndate.setYear(parseInt(date.substring(0, 4)));
        ndate.setDate(parseInt(date.substring(8, 10)));
        ndate.setMonth(parseInt(date.substring(5, 7)-1));


        var newString = user.substring(str+5, str+29);

        var meal = {userid: newString, description: name1, calories: cals, carbs: carbs, fat: fat, protein: protein, date: ndate};

        Meal.insertMany(meal, (err,doc) => {
            if(err){
            }
            res.redirect('/meals');         
        });
    }
    else 
    {
        const params = {
            api_key: process.env.USDA_NDB_API_KEY,
            query: name,
            dataType: ["Survey (FNDDS)"],
            pagesize: 5,
        }
    
        const api_url = 
        `https://api.nal.usda.gov/fdc/v1/foods/search?api_key=${encodeURIComponent(params.api_key)}&query=${encodeURIComponent(params.query)}&dataType=${encodeURIComponent(params.dataType)}&pageSize=${encodeURIComponent(params.pagesize)}&sortOrder=r`
    
        var arr = "";
    
        function getData() {
             return fetch(api_url).then(response => response.json())
        }
    
        var foodArray;
    
        await getData().then(async function(data) {
            foodArray = JSON.parse(JSON.stringify(data.foods));
        })
    
        res.render('addmeal', {
            user: req.user,
            meals: foodArray,
            date: date
        })
        
    }
})


module.exports = router;
