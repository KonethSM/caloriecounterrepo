
const findSunday = (date) =>{
    var curr = new Date(date); // get current date
    if(curr.getDay() === 0){
        return date;
    }
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    
    var sunday = new Date(curr.setDate(first));
    return sunday;
}
const addDays = (date, days) => {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
const getFullDate = (day) => {
    let date = new Date(day);
    return `${date.getMonth() +1}/${date.getDate()}/${date.getFullYear()}`;
}
const getAllWeekDays = (day) =>{
    let week = [];
    let sunday = findSunday(day);
    week[0] = sunday;
    sunday = addDays(sunday,1)
    week[1] = sunday;
    sunday = addDays(sunday,1)
    week[2] = sunday;
    sunday = addDays(sunday,1)
    week[3] = sunday;
    sunday = addDays(sunday,1)
    week[4] = sunday;
    sunday = addDays(sunday,1)
    week[5] = sunday;
    sunday = addDays(sunday,1)
    week[6] = sunday;
    return week;
}
module.exports = {
    getFullDate,
    findSunday,
    getAllWeekDays
}